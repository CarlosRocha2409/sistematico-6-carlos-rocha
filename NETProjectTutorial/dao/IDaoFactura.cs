﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoFactura : IDao<Factura>
    {
        Factura findById(int id);

        void save(Factura f);
        void saveFactura(Factura f, List<Tuple<int, int>> facturas);

        List<Tuple<Factura, List<Tuple<int, int>>>> findAllFacturas();
    }
}
