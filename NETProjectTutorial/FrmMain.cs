﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];

            // new ProductoModel().Populate();
            foreach (Producto p in new ProductoModel().GetProductos())
            {
                DataRow drProducto = dsProductos.Tables["Producto"].NewRow();

                drProducto["Id"] = p.Id;
                drProducto["Sku"] = p.Sku;
                drProducto["Nombre"] = p.Nombre;
                drProducto["Descripcion"] = p.Descripcion;
                drProducto["Cantidad"] = p.Cantidad;
                drProducto["Precio"] = p.Precio;
                drProducto["SKUN"] = p.Sku + " " + p.Nombre;

                dsProductos.Tables["Producto"].Rows.Add(drProducto);
                drProducto.AcceptChanges();
            }

            // new EmpleadoModel().Populate();

            foreach (Empleado empleado in new EmpleadoModel().GetListEmpleado())
            {

                DataRow drEmpleado = dsProductos.Tables["Empleado"].NewRow();

                drEmpleado["Id"] = empleado.Id;
                drEmpleado["INSS"] = empleado.Inss;
                drEmpleado["Cédula"] = empleado.Cedula;
                drEmpleado["Nombres"] = empleado.Nombre;
                drEmpleado["Apellidos"] = empleado.Apellidos;
                drEmpleado["Dirección"] = empleado.Direccion;
                drEmpleado["Teléfono"] = empleado.Tconvencional;
                drEmpleado["Celular"] = empleado.Tcelular;
                drEmpleado["Salario"] = empleado.Salario;
                drEmpleado["Sexo"] = empleado.Sexo;
                drEmpleado["NA"] = empleado.Nombre + " " + empleado.Apellidos;

                dsProductos.Tables["Empleado"].Rows.Add(drEmpleado);
                drEmpleado.AcceptChanges();
            }

            // new ClienteModel().Populate();

            foreach (Cliente client in new ClienteModel().GetListCliente())
            {

                DataRow drCliente = dsProductos.Tables["Cliente"].NewRow();

                drCliente["Id"] = client.Id;
                drCliente["Cedula"] = client.Cedula;
                drCliente["Nombres"] = client.Nombres;
                drCliente["Apellidos"] = client.Apellidos;
                drCliente["Telefono"] = client.Telefono;
                drCliente["Correo"] = client.Correo;
                drCliente["Direccion"] = client.Direccion;
                drCliente["NA"] = client.Nombres + " " + client.Apellidos;

                dsProductos.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
            }

            foreach(Tuple<Factura, List<Tuple<int, int>>> facturaTupla in new FacturaModel().getFacturas()) {

                DataRow drFactura = dsProductos.Tables["Factura"].NewRow();

                drFactura["Id"] = facturaTupla.Item1.Id;
                drFactura["CodFactura"] = facturaTupla.Item1.Cod_factura;
                drFactura["Fecha"] = facturaTupla.Item1.Fecha;
                drFactura["Observaciones"] = facturaTupla.Item1.Observaciones;
                drFactura["Empleado"] = facturaTupla.Item1.Empleado.Id;
                drFactura["Cliente"] = facturaTupla.Item1.Cliente.Id;
                drFactura["SubTotal"] = facturaTupla.Item1.Subtotal;
                drFactura["Iva"] = facturaTupla.Item1.Iva;
                drFactura["Total"] = facturaTupla.Item1.Total;
                dsProductos.Tables["Factura"].Rows.Add(drFactura);
                drFactura.AcceptChanges();

                foreach (Tuple<int, int> productoTupla in facturaTupla.Item2) {

                    DataRow drDetalleFactura = dsProductos.Tables["DetalleFactura"].NewRow();
                    drDetalleFactura["Factura"] = facturaTupla.Item1.Id;
                    drDetalleFactura["Producto"] = productoTupla.Item1;
                    drDetalleFactura["Cantidad"] = productoTupla.Item2;
                    drDetalleFactura["Precio"] = new implements.DaoImplementsProducto().findById(productoTupla.Item1).Precio;

                    dsProductos.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);
                    drDetalleFactura.AcceptChanges();
                }
            }

        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsFacturas = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }
    }
}
