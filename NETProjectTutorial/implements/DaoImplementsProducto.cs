﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.implements
{
    class DaoImplementsProducto : IDaoProducto
    {

        // header producto

        public BinaryReader brhproducto;
        public BinaryWriter bwhproducto;

        // data producto

        public BinaryReader brdproducto;
        public BinaryWriter bwdproducto;

        public FileStream fshproducto;
        public FileStream fsdproducto;

        public const string FILENAME_HEADER = "hproducto.dat";
        public const string FILENAME_DATA = "dproducto.dat";
        public const int SIZE = 305;

        public void open()
        {
            try
            {
                fsdproducto = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshproducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhproducto = new BinaryReader(fshproducto);
                    bwhproducto = new BinaryWriter(fshproducto);

                    brdproducto = new BinaryReader(fsdproducto);
                    bwdproducto = new BinaryWriter(fsdproducto);

                    bwhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhproducto.Write(0);//n
                    bwhproducto.Write(0);//k
                }
                else
                {
                    fshproducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhproducto = new BinaryReader(fshproducto);
                    bwhproducto = new BinaryWriter(fshproducto);
                    brdproducto = new BinaryReader(fsdproducto);
                    bwdproducto = new BinaryWriter(fsdproducto);
                }

            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public void close()
        {
            try
            {
                if (brdproducto != null)
                {
                    brdproducto.Close();
                }
                if (brhproducto != null)
                {
                    brhproducto.Close();
                }
                if (bwdproducto != null)
                {
                    bwdproducto.Close();
                }
                if (bwhproducto != null)
                {
                    bwhproducto.Close();
                }
                if (fsdproducto != null)
                {
                    fsdproducto.Close();
                }
                if (fshproducto != null)
                {
                    fshproducto.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public Producto findById(int idProducto)
        {
            Console.WriteLine(idProducto);
            open();

            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();

            if (idProducto >= n)
            {
                close();
                throw new ArgumentException("No ID was found");
            }
            else
            {

                for (int i = 0; i < n; i++)
                {

                    long hpos = 8 + i * 4;
                    brhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int index = brhproducto.ReadInt32();

                    if (index == idProducto)
                    {

                        long dpos = (index - 1) * SIZE;
                        brdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                        int id = brdproducto.ReadInt32();
                        string sku = brdproducto.ReadString();
                        string nombre = brdproducto.ReadString();
                        string descripcion = brdproducto.ReadString();
                        int cantidad = brdproducto.ReadInt32();
                        double precio = brdproducto.ReadDouble();

                        close();
                        return new Producto(id, sku, nombre, descripcion, cantidad, precio);

                    }

                }
            }

            close();
            return null;
        }


        public void save(Producto p)
        {
            open();

            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();
            int k = brhproducto.ReadInt32();

            long dpos = k * SIZE;
            bwdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdproducto.Write(++k);
            bwdproducto.Write(p.Sku);
            bwdproducto.Write(p.Nombre);
            bwdproducto.Write(p.Descripcion);
            bwdproducto.Write(p.Cantidad);
            bwdproducto.Write(p.Precio);

            bwhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhproducto.Write(++n);
            bwhproducto.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhproducto.Write(k);

            close();
        }

        public int update(Producto p)
        {
            open();

            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();

            for (int i = 0; i < n; i++)
            {

                long hpos = 8 + i * 4;
                brhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhproducto.ReadInt32();

                if (index == p.Id)
                {

                    long dpos = (index - 1) * SIZE;
                    brdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdproducto.Write(p.Id);
                    bwdproducto.Write(p.Sku);
                    bwdproducto.Write(p.Nombre);
                    bwdproducto.Write(p.Descripcion);
                    bwdproducto.Write(p.Cantidad);
                    bwdproducto.Write(p.Precio);

                    close();
                    return 1;

                }

            }

            close();
            return 0;
        }

        public bool delete(Producto t)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findAll()
        {
            open();
            List<Producto> productos = new List<Producto>();

            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();
            for (int i = 0; i < n; i++)
            {

                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhproducto.ReadInt32();

                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdproducto.ReadInt32();
                string sku = brdproducto.ReadString();
                string nombre = brdproducto.ReadString();
                string descripcion = brdproducto.ReadString();
                int cantidad = brdproducto.ReadInt32();
                double precio = brdproducto.ReadDouble();

                Producto c = new Producto(id, sku, nombre, descripcion, cantidad, precio);
                productos.Add(c);

            }

            close();
            return productos;
        }
    }
}
