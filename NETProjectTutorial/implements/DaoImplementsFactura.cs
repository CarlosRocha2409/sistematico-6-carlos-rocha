﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.implements
{
    class DaoImplementsFactura : IDaoFactura
    {

        // header factura

        public BinaryReader brhfactura;
        public BinaryWriter bwhfactura;

        // data factura

        public BinaryReader brdfactura;
        public BinaryWriter bwdfactura;

        public FileStream fshfactura;
        public FileStream fsdfactura;

        public const string FILENAME_HEADER = "hfactura.dat";
        public const string FILENAME_DATA = "dfactura.dat";
        public const int SIZE = 513;

        public void open()
        {
            try
            {
                fsdfactura = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshfactura = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhfactura = new BinaryReader(fshfactura);
                    bwhfactura = new BinaryWriter(fshfactura);

                    brdfactura = new BinaryReader(fsdfactura);
                    bwdfactura = new BinaryWriter(fsdfactura);

                    bwhfactura.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhfactura.Write(0);//n
                    bwhfactura.Write(0);//k
                }
                else
                {
                    fshfactura = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhfactura = new BinaryReader(fshfactura);
                    bwhfactura = new BinaryWriter(fshfactura);
                    brdfactura = new BinaryReader(fsdfactura);
                    bwdfactura = new BinaryWriter(fsdfactura);
                }

            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public void close()
        {
            try
            {
                if (brdfactura != null)
                {
                    brdfactura.Close();
                }
                if (brhfactura != null)
                {
                    brhfactura.Close();
                }
                if (bwdfactura != null)
                {
                    bwdfactura.Close();
                }
                if (bwhfactura != null)
                {
                    bwhfactura.Close();
                }
                if (fsdfactura != null)
                {
                    fsdfactura.Close();
                }
                if (fshfactura != null)
                {
                    fshfactura.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public void saveFactura(Factura f, List<Tuple<int, int>> facturas)
        {
            open();

            brhfactura.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhfactura.ReadInt32();
            int k = brhfactura.ReadInt32();

            long dpos = k * SIZE;
            bwdfactura.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdfactura.Write(++k);
            bwdfactura.Write(f.Cod_factura);
            bwdfactura.Write(f.Fecha.ToString());
            bwdfactura.Write(f.Empleado.Id);
            bwdfactura.Write(f.Cliente.Id);
            bwdfactura.Write(f.Observaciones);
            bwdfactura.Write(f.Subtotal);
            bwdfactura.Write(f.Iva);
            bwdfactura.Write(f.Total);

            // Escritura de cada factura

            bwdfactura.Write(facturas.Count);

            foreach (Tuple<int, int> t in facturas){
                bwdfactura.Write(t.Item1);
                bwdfactura.Write(t.Item2);
            }

            bwhfactura.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhfactura.Write(++n);
            bwhfactura.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhfactura.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhfactura.Write(k);

            close();
        }

        public List<Tuple<Factura, List<Tuple<int, int>>>> findAllFacturas() {

            List<Tuple<Factura, List<Tuple<int, int>>>> facturas = new List<Tuple<Factura, List<Tuple<int, int>>>>();

            open();

            brhfactura.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhfactura.ReadInt32();
            for (int i = 0; i < n; i++)
            {

                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhfactura.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhfactura.ReadInt32();

                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdfactura.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdfactura.ReadInt32();
                string codFactura = brdfactura.ReadString();
                string fecha = brdfactura.ReadString();
                int empleadoId = brdfactura.ReadInt32();
                int clienteId = brdfactura.ReadInt32();
                string observaciones = brdfactura.ReadString();
                double subtotal = brdfactura.ReadDouble();
                double iva = brdfactura.ReadDouble();
                double total = brdfactura.ReadDouble();

                Factura f = new Factura(id, codFactura, DateTime.Parse(fecha), new DaoImplementsEmpleado().findById(empleadoId), new DaoImplementsCliente().findById(clienteId), observaciones, subtotal, iva, total);
                List<Tuple<int, int>> products = new List<Tuple<int, int>>();

                int productCount = brdfactura.ReadInt32();

                for (int j = 0; j < productCount; j++) {
                    products.Add(new Tuple<int, int>(brdfactura.ReadInt32(), brdfactura.ReadInt32()));
                }

                facturas.Add(new Tuple<Factura, List<Tuple<int, int>>>(f, products));

            }

            close();
            return facturas;
        }

        public int update(Factura t)
        {
            throw new NotImplementedException();
        }

        public bool delete(Factura t)
        {
            throw new NotImplementedException();
        }

        public List<Factura> findAll()
        {
            throw new NotImplementedException();
        }

        public Factura findById(int id)
        {
            throw new NotImplementedException();
        }

        public void save(Factura t)
        {
            throw new NotImplementedException();
        }
    }
}
