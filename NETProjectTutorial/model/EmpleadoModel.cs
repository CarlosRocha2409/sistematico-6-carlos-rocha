﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {

        private static List<Empleado> ListEmpleados = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoempleado;

        public EmpleadoModel() {
            daoempleado = new implements.DaoImplementsEmpleado();
        }

        public List<Empleado> GetListEmpleado()
        {
            return daoempleado.findAll();
        }

        public void Populate()
        {
            ListEmpleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));
            foreach (Empleado e in ListEmpleados)
            {
                daoempleado.save(e);
            }
        }

        public Empleado DataRowToEmpleado(DataRow dataRowEmpleado)
        {
            Empleado e = new Empleado();

            e.Id = Convert.ToInt32(dataRowEmpleado["Id"].ToString());
            e.Nombre = dataRowEmpleado["Nombres"].ToString();
            e.Apellidos = dataRowEmpleado["Apellidos"].ToString();
            e.Direccion = dataRowEmpleado["Dirección"].ToString();
            e.Tconvencional = dataRowEmpleado["Teléfono"].ToString();
            e.Tcelular = dataRowEmpleado["Celular"].ToString();
            e.Inss = dataRowEmpleado["INSS"].ToString();
            e.Cedula = dataRowEmpleado["Cédula"].ToString();
            e.Salario = Convert.ToDouble(dataRowEmpleado["Salario"].ToString());
            e.Sexo = (dataRowEmpleado["Sexo"].ToString().Equals("Masculino")) ? Empleado.SEXO.MALE : Empleado.SEXO.FEMALE;
            return e;
        }

        public void save(DataRow dataRowEmpleado)
        {
            daoempleado.save(this.DataRowToEmpleado(dataRowEmpleado));
        }

        public void update(DataRow dataRowEmpleado)
        {
            daoempleado.update(this.DataRowToEmpleado(dataRowEmpleado));
        }


    }
}
