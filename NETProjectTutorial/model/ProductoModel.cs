﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {

        private static List<Producto> productos = new List<Producto>();
        private implements.DaoImplementsProducto daoProducto;

        public ProductoModel() {
            daoProducto = new implements.DaoImplementsProducto();
        }

        public List<Producto> GetProductos()
        {
            return daoProducto.findAll();
        }

        public void Populate()
        {
            productos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));
            foreach (Producto p in productos)
            {
                daoProducto.save(p);
            }
        }

        public Producto DataRowToProducto(DataRow dataRowProducto)
        {
            Producto p = new Producto();

            p.Id = Convert.ToInt32(dataRowProducto["Id"].ToString());
            p.Sku = dataRowProducto["SKU"].ToString();
            p.Nombre = dataRowProducto["Nombre"].ToString();
            p.Descripcion = dataRowProducto["Descripcion"].ToString();
            p.Cantidad = Convert.ToInt32(dataRowProducto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(dataRowProducto["Precio"].ToString());

            return p;
        }

        public void save(DataRow dataRowProducto)
        {
            daoProducto.save(this.DataRowToProducto(dataRowProducto));
        }

        public void update(DataRow dataRowProducto)
        {
            daoProducto.update(this.DataRowToProducto(dataRowProducto));
        }
    }
}
