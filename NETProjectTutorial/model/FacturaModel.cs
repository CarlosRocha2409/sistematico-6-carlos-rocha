﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class FacturaModel
    {

        private implements.DaoImplementsFactura daoFactura;

        public FacturaModel()
        {
            daoFactura = new implements.DaoImplementsFactura();
        }

        public List<Tuple<Factura, List<Tuple<int, int>>>> getFacturas()
        {
            return daoFactura.findAllFacturas();
        }

        public Factura DataRowToFactura(DataRow dataRowFactura)
        {
            Factura f = new Factura();
            f.Id = Convert.ToInt32(dataRowFactura["Id"].ToString());
            f.Cod_factura = dataRowFactura["CodFactura"].ToString();
            f.Fecha = DateTime.Parse(dataRowFactura["Fecha"].ToString());
            f.Empleado = new implements.DaoImplementsEmpleado().findById(Convert.ToInt32(dataRowFactura["Empleado"]));
            f.Cliente = new implements.DaoImplementsCliente().findById(Convert.ToInt32(dataRowFactura["Empleado"]));
            f.Observaciones = dataRowFactura["Observaciones"].ToString();
            f.Subtotal = Convert.ToDouble(dataRowFactura["Subtotal"]);
            f.Iva = Convert.ToDouble(dataRowFactura["IVA"]);
            f.Total = Convert.ToDouble(dataRowFactura["Total"]);
            return f;
        }

        public void save(DataRow factura, List<Tuple<int, int>> productos) {
            daoFactura.saveFactura(DataRowToFactura(factura), productos);
        }
    }
}
