﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {

        private static List<Cliente> ListClientes = new List<Cliente>();
        private implements.DaoImplementsCliente daocliente;

        public ClienteModel()
        {
            daocliente = new implements.DaoImplementsCliente();
        }

        public List<Cliente> GetListCliente()
        {
            return daocliente.findAll();
        }

        public void Populate()
        {
            ListClientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Cliente_data));
            foreach(Cliente c in ListClientes)
            {
                daocliente.save(c); 
            }
        }

        public Cliente DataRowToCliente(DataRow dataRowCliente) {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(dataRowCliente["Id"].ToString());
            c.Cedula = dataRowCliente["Cedula"].ToString();
            c.Nombres = dataRowCliente["Nombres"].ToString();
            c.Apellidos = dataRowCliente["Apellidos"].ToString();
            c.Telefono = dataRowCliente["Telefono"].ToString();
            c.Correo = dataRowCliente["Correo"].ToString();
            c.Direccion = dataRowCliente["Direccion"].ToString();
            return c;
        }

        public void save(DataRow dataRowCliente)
        {
            daocliente.save(this.DataRowToCliente(dataRowCliente));
        }

        public void update(DataRow dataRowCliente) {
            daocliente.update(this.DataRowToCliente(dataRowCliente));
        }

        public Cliente findById(int id) {
            return daocliente.findById(id);
        }

    }
}
